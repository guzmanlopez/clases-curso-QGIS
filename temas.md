Curso QGIS 1
===
- **Duración:** 1 mes
- **Número de clases:** 8
- **Frecuencia:** 2 clases por semana
- **Horas por clase:** 3
- **Público:** funcionarios de la DINARA

### Clase 1

**Teórico**

- Presentación del curso QGIS I: alcance y objetivos

- Introducción a los Sistemas de Información Geográfica (SIG):
 - ¿Qué es un SIG?
 - ¿Para qué sirven?
 - Tipos de Software (código abierto, libre y privativos)
 - Artículo 1 y 2 de la Ley No 19.179 “Software libre y
formatos abiertos en el estado”
 - Diferentes SIG libres (QGIS, GRASS, gvSIG, SAGA, etc.)
 - Fuentes de datos y formatos: texto, shapefiles, base de datos, web
 - Capas espaciales vectoriales: puntos, lineas, polígonos
 - Capas espaciales raster
 - Sistemas de Coordenadas de Referencia (CRS) y Proyecciones

- Introducción al QGIS

 - Interfaz general de usuario (GUI)
 - Crear un nuevo proyecto
 - Estructura de carpetas y archivos sugerida para cada proyecto
 - Definir proyecciones de proyecto y de las capas espaciales
 - Cargar capas vectoriales (texto y shapefiles)
 - Formato de archivos de texto
 - Propiedades de la capas vectoriales: tabla de atributos, estilo, etiquetas, campos
 - Filtros (consultas)
 - Selector de elementos, medir distancias, navegación, zoom
 - Visibilidad: orden de capas
 - Visibilidad: transparencia de capas y transparencia de color de relleno
 - Copiar y pegar estilos entre capas
 - Duplicado de capa
 - Grupos de capas ej. Pesca, Oceanografía, Referencias
 - Cargar capa raster de SST
 - Propiedades de las capas raster: estilos, valores máx y min
 - Modo edición de capas: seleccionar elementos, borrar, modificar nodos

**Práctico**

Objetivos: familiarizarse con la interfaz del programa, cargar distintos tipos de datos espaciales, definir CRS y proyecciones del proyecto y las capas, y personalizar estilos de visualización.

*Datos del P1*

- Lances de pesca Aldebarán Costera 2005-03 (archivo csv de puntos)
- Isóbatas (ESRI Shapefile de líneas)
- Zonas jurídicas (archivo ESRI Shapefile de polígonos)
- Uruguay con departamentos (ESRI Shapefile de polígonos, proyección UTM Zona 21S)
- SST Setiembre 2005 (Raster GeoTiff)

---

### Clase 2

- Modificación del proyecto del Práctico 1:
- Cambiar colores y tamaños de símbolos según atributos, filtrar datos de alguna capa y editar eliminando elementos

**Teórico**

- QGIS: impresión de mapas y complementos

 - Diferencias entre mapas y sistemas de información geográfica
 - Elementos presentes en un mapa: leyenda, escala, grilla, coordenadas, vista general, etc.
 - Complementos (plugins): que son, quién los hace, potencial e instalación
 - Capas base: fuentes
 - Cargar capas desde la web: complemento Openlayers
 - Cargar capas desde OceanColor Downloader

**Práctico**

 Objetivos: 1) impresión y personalización de mapas, 2) instalar y utilizar complementos del QGIS

 *Datos del P2*

Parte 1
 - Cargar proyecto Práctico 1
 - Impresión de mapas: mostrar capas visibles, configurar leyenda, agregar imagen logo DINARA, referencias de latitud y longitud, escala, vista general de mapa
 - Obtener mapa completo con símbolos de tamaño en función de las capturas en cada lance para una especie

Parte 2
 - Crear proyecto nuevo
 - Lances de pesca Aldebarán (archivo csv de puntos)
 - Cargar capa base desde Openlayers (Google Maps -> Google Hybrid)
 - Cargar SST desde OceanColor Downloader

---

### Clase 3

- Repaso de la primer semana (Clases 1 y 2)

**Teórico**

- Cargar capas desde OpenStreetMaps: complemento QuickOSM
- Consultas espaciales (puntos en polígonos):
 - Editar en función de la consulta espacial
 - Clusters y Mapa de calor para visualizar densidad de puntos en el espacio (exploratorios):
   - complemento Marker Cluster
   - herramienta Raster Mapa de calor

**Práctico**

- Crear grupo Referencias y cargar las siguientes capas:
- Cargar desde Openlayers un mapa de OpenStreetMaps (navegar a Uruguay y usar Amplificador 150%)
- Consultas QuickOSM:
    - Admin L2 (borde país)
    - Admin L4 (borde departamentos)
    - Nature - Coastline
    - Cortar polígono Río Grande do Sul:
      - Vectorial -> Herramientas de Geometría: convertir a polígono la línea de costa y luego editar nodos
      - Vectorial -> Herramientas de Geoproceso: seleccionar polígono Rio Grande do Sul y hacer *Diferencia* con poligono recien creado
      - Eliminar Santa Catarina y Rio Grande Do Sul de capa original
      - Guardar el polígono en memoria de Rio Grande do Sul
      - Combinar capas vectoriales
      - Eliminar archivos de Poligono de Rio Grande Do Sul
      - Guardar combinado (OSM_Admin4_poli.shp)
      - Place - City (ciudades)
      - Waterway - River
  - Aplicar estilos predefinidos a capas

- Cargar puntos de partes de pesca a partir de CSV
- Guardar como ESRI Shapefile
- Modo editar capa y hacer consulta espacial para seleccionar las posiciones de barcos dentro de polígono de tierra y apretar eliminar
- Raster: Mapa de calor de concentración de puntos: 0.125 unidades de mapa (cuarto de grado)
- Vectorial: mapa de calor de concentración de puntos
- Cambiar estilo de la capa OSM_NatureWater_poli copiar con not( "name" = 'Río de la Plata') (relleno transparento al Río de la Plata)
- Cluster de puntos: seleccionar todos los puntos y guardar como shapefile y aplicar complemento

---

### Clase 4

**Teórico**

- Cuantificación
- Operaciones con tablas: Calculadora de campos
- Operaciones con grillas
 - Definir grillas
 - Contar puntos en polígono
 - Complemento "Point Sampling Tool" (unir información en función de consulta espacial)

**Práctico**

- Cargar Proyecto 3
- Calcular rendimiento de Angelito (nro cajas * 25 kg sobre tiempo en horas arrastre (kg/h))
- Vectorial: representar mapa de calor dinámico ponderado por el rendimiento de Angelito
- Editar outlier
- Construir cuadrantes estadísticos: 0.25º x 0.25º: Vectorial -> Herramientas de Investigación -> Cuadrícula vectorial (extent de capa partes y modificar así: -59,-51,-37,-31), espaciado según resolución en grados
- Calcular puntos por cuadrado: Vectorial -> Herramientas de análisis -> Contar puntos en polígono (guardar capa)
- Aplicar estilo, etiquetar por ID para referencia de los cuadrantes
- Pedir que analicen el mapa para descubri que hay puntos que caen en intersección y no son contados
- Hacer de nuevo con grilla corrida: -59.0100,-51.0100,-37.0100,-31.0100
- Complemento Group Stats: estadísticas de Rendimiento de Angelito agrupando por ID (guardar CSV)
- Complemento mmQGIS -> Combine -> Attributes join from CSV file (unir grilla con CSV guardado por ID)
- Complemento mmQGIS -> Modify -> Text to Float
- Analizar mapa de rendimiento de Angelito
- Ver estacionalidad (filtrar fechas) para la actividad de la flota y el rendimiento de angelito mediante heatmap

---

### Clase 5

- Repaso de la segundo semana (Clases 3 y 4)

**Teórico**

- Modelos de elevación digital (DEMs)
- Extraer isolíneas desde ráster
- Extraer isolíneas desde capa de puntos
- Métodos de generalización: complemento Generalizer
  - eliminar objetos pequeños
  - simplificar
  - suavizar

**Práctico**

- Cargar referencias
- Cargar GEBCO 1 min y aplicarle estilo de colores azul
- Crear raster de pendiente en porcentaje -> Raster -> Análisis -> MDT
- Visualizar batimetría con transparencia para ver capa de pendiente
- Crear isóbatas cada 100 metros, columna PROF
- Aplicar estilo (Configuración Estilos, cpt-city, batimetría) y etiquetar
- Cargar capa raster SST
- Aplicar estilo (Configuración Estilos, cpt-city, temperatura) y etiquetar
- Crear isotermas cada 1 grado, columna TEMP
- Reproyectar capa raster SST: 600 celdas x 300 celdas pasar a 75 x 36 celdas
- Copiar y pegar estilo sobre capa
- Crear isotermas cada 1 grado, columna TEMP
- Complemento Generalizer: eliminar objetos menor a 2 grados, simplificar con Chaiken con valores por defecto

---

### Clase 6

**Teórico**

- Buffers
- Animación en capas espaciales con tiempo: complemento TimeManager

**Práctico**

- AIS flota
- VMS Cherna
- Buffers en montículos de coral de aguas profundas

---

### Clase 7

- Repaso de la tercer semana (Clases 5 y 6)
- Repaso general
- Consulta de dudas general

**Teórico**

- Digitalización de capas

**Práctico**

- Digitalizar carta nautica impresa

---

### Clase 8

- Evaluación final del curso
- Evaluación y sugerencias sobre el curso por parte de los participantes

---

### Recursos

En español:
http://www.qgistutorials.com/es/index.html
