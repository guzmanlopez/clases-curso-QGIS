# Práctico 3 - Complemento QuickOSM, consultas espaciales y estilos predefinidos

<img style="float:right; width: 10%;" src="../../fig/qgis-icon.svg">

## Objetivos

1. Obtener capas vectoriales de referencia desde Base de Datos abiertas
2. Realizar consultas espaciales
3. Aplicar estilos predefinidos a capas

## Temas

### Parte 1

Descargar capas vectoriales desde la Base de Datos abierta Open Street Maps (OSM) mediante el complemento QuickOSM, combinar varias capas vectoriales en una sola y aplicar estilos predefinidos

1. Crear grupo Referencias y cargar las siguientes capas:
2. Cargar desde OpenLayers un mapa de OpenStreetMaps (navegar a Uruguay)
3. Consultas QuickOSM (*Vectorial* -> *QuickOSM*):
 - **Borde de departamentos, provincias o estados:** *Admin L4*
 - **Línea de costa:** *Natural - Coastline*
 - **Ciudades:** *Place - City*
 - **Ríos y arroyos:** *Waterway - River*
 - **Espejos de agua:** *Natural - Water*
 - **Uso del suelo:** *Landuse - vacío*
4. Combinar capas vectoriales (*Vectorial* -> *Herramientas de gestión de datos*)
5. Aplicar estilos predefinidos a capa de Ríos y arroyos y a capa de Espejos de agua (ver carpeta *Estilos*)

### Parte 2

Generar consultas espaciales entre capas vectoriales (*puntos en polígonos*) y editar en función de la consulta

1. Cargar puntos de partes de pesca categoría B año 2012 a partir de archivo CSV
2. Guardar como ESRI Shapefile
3. Consulta espacial (*Vectorial* -> *Consulta espacial*):
 - Seleccionar las posiciones de barcos dentro del polígono de tierra
4. Consulta espacial y edición:
 - Seleccionar modo edición de capas vectoriales
 - Realizar consulta espacial previa
 - Eliminar puntos seleccionados

## Datos utilizados

- Polígonos de departamentos, provincias y estados (OSM)
- Líneas de Costa (OSM)
- Puntos de ciudades (OSM)
- Líneas de Ríos y arroyos (OSM)
- Polígonos de Espejos de agua (OSM)
- Polígonos de uso de suelo (OSM)
- Partes de pesca categoría B año 2012 (Partes2012_catB.csv)
- Archivos de estilos predefinidos (*osm_spatialite_googlemaps_lines.qml* y *osm_spatialite_googlemaps_multipolygon.qml*)
