# Práctico 1 - Introducción al QGIS

<img style="float:right; width: 10%;" src="../../fig/qgis-icon.svg">

## Objetivos

- Familiarizarse con la interfaz del programa, cargar distintos tipos de datos espaciales, definir CRS y proyecciones del proyecto y las capas, y personalizar estilos de visualización.

## Temas

 1. Interfaz general de usuario (GUI)
 2. Crear un nuevo proyecto
 3. Estructura de carpetas y archivos sugerida para cada proyecto
 4. Definir proyecciones de proyecto y de las capas espaciales
 5. Cargar capas vectoriales (texto y shapefiles)
 6. Formato de archivos de texto
 7. Propiedades de la capas vectoriales: tabla de atributos, estilo, etiquetas, campos
 8. Filtros (consultas)
 9. Selector de elementos, medir distancias, navegación, zoom
 10. Visibilidad: orden de capas
 11. Visibilidad: transparencia de capas y transparencia de color de relleno
 12. Copiar y pegar estilos entre capas
 13. Duplicado de capa
 14. Grupos de capas ej. Pesca, Oceanografía, Referencias
 15. Cargar capa raster de SST
 16. Propiedades de las capas raster: estilos, valores máx y min
 17. Modo edición de capas: seleccionar elementos, borrar, modificar nodos


## Datos utilizados

- Lances de pesca Aldebarán Costera 2005-03 (archivo csv de puntos)
- Isóbatas (ESRI Shapefile de líneas)
- Zonas jurídicas (archivo ESRI Shapefile de polígonos)
- Uruguay con departamentos (ESRI Shapefile de polígonos, proyección UTM Zona 21S)
- SST Setiembre 2005 (Raster GeoTiff)
