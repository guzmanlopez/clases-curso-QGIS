# Práctico 2 - Impresión de mapas y complementos

<img style="float:right; width: 10%;" src="../../fig/qgis-icon.svg">

## Objetivos

1. Instalar y utilizar complementos del QGIS
2. Impresión y personalización de mapas

## Temas

### Parte 1

1. Crear proyecto nuevo
2. Convertir archivo *CosteraSIG17.xls* a *CosteraSIG17.csv*
2. Cargar lances de pesca Aldebarán (archivo csv de puntos *CosteraSIG17.csv*)
3. Complementos QGIS:
 - Instalar complementos Openlayers y OceanColor Downloader
 - Cargar capa base con complemento Openlayers (Google Maps -> Google Hybrid)
 - ~~Cargar Temperatura Superficial del Mar (SST) con el complemento OceanColor Downloader~~
 - Descargar imagen de Temperatura Superficial del Mar (SST) desde la página web de la NASA - OceanColor (https://oceancolor.gsfc.nasa.gov/):
    - DATA -> Level 3 Browser
      - Standar Products
      - TERRA MODIS Sea Surface Temperature (11 u nighttime)
      - Monthly
      - 4 km
      - Sep05
      - SMI HDF

### Parte 2

1. Cargar proyecto del Práctico 1
2. Impresión de mapas:
 - mostrar capas visibles
 - configurar leyenda
 - agregar imagen logo DINARA
 - referencias de latitud y longitud
 - escala
 - vista general de mapa
3. Obtener mapa completo con símbolos de tamaño en función de las capturas en cada lance para una especie

## Datos utilizados

- Google Maps (Raster desde web)
- Temperatura Superficial del Mar (SST) (Raster GeoTiff)
- Proyecto 1 (Archivo *.qgs* con las capas del Proyecto 1)
